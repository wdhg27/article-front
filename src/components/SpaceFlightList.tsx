import * as React from "react";

import { SpaceflightResponse } from "../types/SpaceflightResponse";
import SpaceFlightItem  from "./SpaceFlightItem";

import InfiniteScroll from "react-infinite-scroll-component";
import axios from "axios";

export const SpaceFlightList = () => {
  const [flights, setFlights] = React.useState<Array<SpaceflightResponse>>([]);
  const [count, setCount] = React.useState<number>(0);
  const [loading, setLoading] = React.useState(true);

  const showedFlights: SpaceflightResponse[] = React.useMemo(
    () => flights.slice(0, count),
    [count]
  );

  React.useEffect(() => {
    axios.get("http://localhost:4000/cacheResponse/cache").then((res) => {
      setFlights(res.data);
      showMoreFlights();
    });
  }, []);

  const showMoreFlights = () => {
    setCount(count + 10);
    setLoading(false);
  };

  const loadingComponent = () => <h4>Loading More Flights...</h4>;

  return !loading ? (
    <InfiniteScroll
      dataLength={showedFlights.length}
      next={() => showMoreFlights()}
      hasMore={showedFlights.length < 100}
      loader={
        showedFlights.length > 100 ? (
          <h4>Loading More Flights...</h4>
        ) : (
          <h4>There are no more flights</h4>
        )
      }
    >
      {showedFlights.length ? (
        showedFlights.map((flight, index) =>
          SpaceFlightItem({ data: flight, index })
        )
      ) : (
        <p>There are no flights</p>
      )}
    </InfiniteScroll>
  ) : (
    loadingComponent()
  );
};
