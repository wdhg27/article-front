import React from "react";
import { SpaceflightResponse } from "../types/SpaceflightResponse";

export interface SpaceflightItemProps {
  data: SpaceflightResponse;
  index: number;
}

const SpaceFlightItem = ({ data, index }: SpaceflightItemProps) => {
  return (
    <React.Fragment key={index}>
      <a
        href={data.url}
        target="_blank"
        rel="noopener noreferrer"
        data-testid="space-flight-link"
      >
        {index} - {data.title}
      </a>
      <br />
      <img
        src={data.imageUrl}
        alt={data.title}
        style={{ maxWidth: 300 }}
        data-testid="space-flight-image"
      />
      <br />
    </React.Fragment>
  );
};

export default SpaceFlightItem;