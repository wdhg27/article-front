import "./App.css";

import { SpaceFlightList } from "./components/SpaceFlightList";

function App() {
  return (
    <div data-testid="space-flight-app">
      <SpaceFlightList />
    </div>
  );
}

export default App;