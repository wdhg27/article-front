// test("1", () => {
//   expect("1").toBe(1);
// });

import React from "react";
// Exten DOM capacities
import "@testing-library/jest-dom/extend-expect";
// Method to render React component
import { render } from "@testing-library/react";

import SpaceFlightItem, { SpaceflightItemProps } from "./SpaceFlightItem";

describe("SpaceFlightItem", () => {
  test("renders SpaceFlightItem component", () => {
    const props: SpaceflightItemProps = {
      data: {
        imageUrl: "",
        title: "",
        url: "",
      },
      index: 1,
    };

    const component = render(
      <SpaceFlightItem data={props.data} index={props.index} />
    );

    const spaceFlightLink = component.getByTestId("space-flight-link");
    const spaceFlightImage = component.getByTestId("space-flight-image");

    expect(spaceFlightLink).toBeInTheDocument();
    expect(spaceFlightImage).toBeInTheDocument();
  });
});